image : lorisleiva/laravel-docker:latest

stages :
  - build
  - test
  - deploy

composer : 
  stage: build
  cache:
    key: ${CI_COMMIT_REF_SLUG}-composer
    paths: 
    - vendor /

  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.example .env
    - php artisan key:generate
  artifacts:
    expire_in: 1 month
    paths:
      - vendor/
      - .env

npm :
  stage: build
  cache:
    key: ${CI_COMMIT_REF_SLUG}-npm
    paths:
      - node_modules/
  script:
    - npm install
    - npm run production
  artifacts:
    expire_in: 1 month
    paths:
      - node_modules/
      - public/css/
      - public/js/

phpunit:
  stage: test
  dependencies:
    - composer
  script:
    - phpunit --coverage-text --colors=never

codestyle: 
  dependencies: []
  script:
    - phpcs --standard=PSR2 --extensions=php --ignore=app/Support/helpers.php app

# No Variables for Staging and deployment